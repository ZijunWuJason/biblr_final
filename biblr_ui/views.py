from flask import (
    request, session, g, redirect, url_for, abort,
    render_template, flash
)
import requests

from .biblr_ui import app

from . import books


@app.context_processor
def inject_books():
    return {'books': books}


@app.route('/')
def index():
    return render_template('index.html')


def setup_show_verses(book, chapter, verse):
    g.book = book
    g.chapter = chapter
    g.verse = verse

    # Build the breadcrumb
    bc = [({'book':book}, books.BOOK_LUT[book])]
    if chapter is not None:
        bc.append(({'chapter': chapter, 'book': book}, chapter + 1))
    if verse is not None:
        bc.append(({'chapter': chapter, 'book': book, 'verse': verse},
                   verse + 1))
    g.breadcrumb = [(url_for('show_verses',**kw), v) for kw,v in bc]

    # Grab the verses from the database
    if chapter and verse:
        verses = requests.get(
            '{}/verses/{}/{}/{}'.format(
                app.config['BIBLR_REST'], book, chapter, verse
            )
        ).json()
    elif chapter:
        verses = requests.get(
            '{}/verses/{}/{}'.format(app.config['BIBLR_REST'], book, chapter)
        ).json()
    else:
        verses = requests.get(
            '{}/verses/{}'.format(app.config['BIBLR_REST'], book)
        ).json()
    g.verses = verses

@app.route('/favicon.ico')
def favicon():
    return ''

@app.route('/<book>')
@app.route('/<book>/<int:chapter>')
@app.route('/<book>/<int:chapter>/<int:verse>')
def show_verses(book, chapter=None, verse=None):
    setup_show_verses(book, chapter, verse)

    comments_raw = requests.get(
        '{}/comments'.format(app.config['BIBLR_REST'])
    ).json()
    app.logger.info(comments_raw)
    comments = {
        c['verse_id']: []
        for c in comments_raw
    }
    for c in comments_raw:
        comments[c['verse_id']].append(c)

    app.logger.info(comments)
    return render_template(
        'show_verses.html', comments=comments,
    )

@app.route('/search', methods=['POST'])
def search():
    return redirect(url_for('index'))


@app.route('/add', methods=['POST'])
def add_comment():
    app.logger.info('asdf')
    if not session.get('logged_in'):
        abort(401)
    book,chapter,verse,text = map(request.form.get,
                                  ['book','chapter','verse','text'])
    chapter = int(chapter) - 1
    verse = int(verse) - 1
    
    # db = get_db()
    # db.execute('insert into comments (book,chapter,verse,text)'
    #            ' values (?, ?, ?, ?)',
    #            [book, chapter, verse, text])
    # db.commit()
    # flash('New entry was successfully posted')
    return redirect(request.args.get('next') or
                    request.referrer or url_for('index'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['username']
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
            origin = request.args.get('origin')
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
            origin = request.args.get('origin')
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(request.args.get('origin'))
    origin = request.referrer
    return render_template('login.html', error=error, origin=origin)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(request.referrer)
