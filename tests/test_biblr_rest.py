import os
import sys
import unittest
import tempfile
import json
import string
import random

import requests


try:
    from biblr_rest import app, db, api
    import biblr_rest.models as models
    from biblr_rest.models import get_verses, get_users
except:
    pass

    
def asdict(o, cols):
    return {c: getattr(o, c) for c in cols}

class BiblrRestTestCase(unittest.TestCase):
    def setUp(self):
        random.seed(0)
        self.temp_dir = tempfile.TemporaryDirectory()
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(
            os.path.join(self.temp_dir.name, 'biblr.db')
        )
        app.testing = True
        self.client = app.test_client()
        db.init_app(app)
        api.init_app(app)
        with app.app_context():
            db.create_all()

    def tearDown(self):
        self.temp_dir.cleanup()

    def populate_verses(self):
        verse_gen = get_verses()
        verses = [next(verse_gen) for i in range(10)]
        db.session.add_all(models.Verse(**v) for v in verses)
        return [asdict(v, ['id','book','chapter','verse','text'])
                for v in models.Verse.query.all()]

    def populate_users(self):
        user_gen = get_users()
        users = [next(user_gen) for i in range(10)]
        db.session.add_all(models.User(**v) for v in users)
        return [asdict(u, ['id','name','phone','email','password'])
                for u in models.User.query.all()]

    def populate_comments(self):
        verses = self.populate_verses()
        users = self.populate_users()
        comments = []
        for i,u in enumerate(users[:5]):
            for j,v in enumerate(verses[:5]):
                comments.append(
                    {'text': ''.join(random.sample(string.ascii_letters, 30)),
                     'user_id': i+1, 'verse_id': j+1}
                )
        db.session.add_all(models.Comment(**c) for c in comments)
        return (verses, users,
                [asdict(c, ['id', 'text','user_id','verse_id'])
                 for c in models.Comment.query.all()])

    def test_00100_verse_model_existence(self):
        assert models.Verse

    def test_00110_verse_model_base(self):
        assert issubclass(models.Verse, db.Model)

    def test_00200_user_model_existence(self):
        assert models.User

    def test_00210_user_model_base(self):
        assert issubclass(models.User, db.Model)

    def test_00300_comment_model_existence(self):
        assert models.Comment

    def test_00310_comment_model_base(self):
        assert issubclass(models.Comment, db.Model)

    def test_00400_verse_model_attrs(self):
        assert models.Verse.id
        assert models.Verse.book
        assert models.Verse.chapter
        assert models.Verse.verse
        assert models.Verse.text
        assert models.Verse.comments

    def test_00410_user_model_attrs(self):
        assert models.User.id
        assert models.User.name
        assert models.User.email
        assert models.User.phone
        assert models.User.password
        assert models.User.comments

    def test_00420_comment_model_attrs(self):
        assert models.Comment.id
        assert models.Comment.verse_id
        assert models.Comment.user_id
        assert models.Comment.user
        assert models.Comment.verse

    def test_00500_verse_model_insert(self):
        with app.app_context():
            self.populate_verses()
            db.session.commit()

    def test_00510_user_model_insert(self):
        with app.app_context():
            self.populate_users()
            db.session.commit()

    def test_00520_comment_model_insert(self):
        with app.app_context():
            self.populate_comments()
            db.session.commit()

    def test_00600_verse_api_get(self):
        with app.app_context():
            verses = self.populate_verses()
            db.session.commit()
        for i,v in enumerate(verses):
            rv = self.client.get('/verses/{}'.format(i + 1))
            data = json.loads(rv.data)
            # data.pop('id')
            self.assertEqual(data, dict(v))

    def test_00610_verse_by_book_api_get(self):
        with app.app_context():
            verses = self.populate_verses()
            db.session.commit()
        rv = self.client.get('/verses/ge')
        assert verses == json.loads(rv.data)

    def test_00620_verse_by_chapter_api_get(self):
        with app.app_context():
            verses = self.populate_verses()
            db.session.commit()
        rv = self.client.get('/verses/ge/0')
        assert [v for v in verses if v['chapter']==0] == json.loads(rv.data)

    def test_00630_verse_by_verse_api_get(self):
        with app.app_context():
            verses = self.populate_verses()
            db.session.commit()
        rv = self.client.get('/verses/ge/0/0')
        assert ([v for v in verses if v['chapter']==0 and v['verse']==0] ==
                json.loads(rv.data))

    def test_00710_user_api_get(self):
        with app.app_context():
            users = self.populate_users()
            db.session.commit()
        for i,v in enumerate(users):
            rv = self.client.get('/users/{}'.format(i + 1))
            data = json.loads(rv.data)
            # data.pop('id')
            self.assertEqual(data, dict(v))

    def test_00720_user_list_api_get(self):
        with app.app_context():
            users = self.populate_users()
            db.session.commit()
        assert users == json.loads(self.client.get('/users').data)

    def test_00730_verse_list_api_get(self):
        with app.app_context():
            verses = self.populate_verses()
            db.session.commit()
        assert verses == json.loads(self.client.get('/verses').data)

    def test_00740_comment_list_api_get(self):
        with app.app_context():
            verses, users, comments = self.populate_comments()
            db.session.commit()
        assert comments == json.loads(self.client.get('/comments').data)

    def test_00750_comment_api_get(self):
        with app.app_context():
            verses, users, comments = self.populate_comments()
            db.session.commit()
        for i,c in enumerate(comments):
            rv = self.client.get('/comments/{}'.format(i + 1))
            data = json.loads(rv.data)
            assert data == c

    def test_00810_user_comments_api_get(self):
        with app.app_context():
            verses, users, comments = self.populate_comments()
            db.session.commit()
        for i,u in enumerate(users[:5]):
            rv = self.client.get('/users/{}/comments'.format(i + 1))
            data = json.loads(rv.data)
            assert ({c['text'] for c in comments[i*5:(i*5)+5]} ==
                    {d['text'] for d in data})

    def test_00910_verse_comments_api_get(self):
        with app.app_context():
            verses, users, comments = self.populate_comments()
            db.session.commit()
        for i,v in enumerate(verses[:5]):
            rv = self.client.get('/verses/{}/comments'.format(i + 1))
            data = json.loads(rv.data)
            assert ({c['text'] for c in comments[i::5]} ==
                    {d['text'] for d in data})

    def test_01010_verse_search_api(self):
        with app.app_context():
            verses = self.populate_verses()
            db.session.commit()
        for i,v in enumerate(verses):
            w = v['text'].split()[0]
            rv = self.client.post(
                '/search/verses', data=dict(
                    text='%{}%'.format(w)
                )
            )
            data = json.loads(rv.data)
            assert v in data
    
    def test_01110_user_search_api(self):
        with app.app_context():
            users = self.populate_users()
            db.session.commit()
        for i,u in enumerate(users):
            rv = self.client.post('/search/users', data={'name': u['name']})
            data = json.loads(rv.data)
            assert u == data
    
    def test_01210_comment_search_api(self):
        with app.app_context():
            verses, users, comments = self.populate_comments()
            db.session.commit()
        for i,c in enumerate(comments):
            w = c['text'][:5]
            rv = self.client.post(
                '/search/comments', data={'text':'%{}%'.format(w)},
            )
            data = json.loads(rv.data)
            assert c in data
    

if __name__ == '__main__':
    unittest.main()